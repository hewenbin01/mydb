package cn.com.dao;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import cn.com.mydb.DBPool;
import cn.com.mydb.DbManager;

/** 
 * <p>项 目：MYDB</p>
 * <p>模 块：</p>
 * <p>描 述：</p>
 * <p>版 权: Copyright (c) 2016</p>
 * <p>公 司: 天玑科技</p>
 * @author hewenbin
 * @version 1.0 
 * 创建时间：2016年10月26日 下午2:27:38
 */
public class TestDao {

	@Before
	public void before() {
		System.out.println("…………初始化操作开始启动…………");
		new DBPool().init();
		DbManager.LoadSqlMap();
	}

	@Ignore
	public void destroy() {
		System.out.println("…………释放连接池…………");
		new DBPool().release();
	}
	
	@Ignore
	public void test1(){
		for (int i = 0; i < 10; i++) {
			UserDao dao = new UserDao();
			dao.tt();
		}
	}
	
	@Test
	public void test(){
		for (int i = 0; i < 1; i++) {
			System.out.println(i);
//		   Thread thread =	new Thread(new TestThread());
//		    thread.start();
			new Thread(new Runnable() {
				
				public void run() {
					int a=0;
					while(true){
						System.out.println(a++);
					}
				}
			}).start();
			
			System.out.println("aaaaaaaaaaaaaaaaaaaaaaa");
		}
	}
	
	public static void main(String[] args) {
		new DBPool().init();
		DbManager.LoadSqlMap();
		for (int i = 0; i < 10; i++) {
			System.out.println(i);
		   Thread thread =	new Thread(new TestThread());
		    thread.start();
//			new Thread(new Runnable() {
//				
//				public void run() {
//					int a=0;
//					while(true){
//						System.out.println(a++);
//					}
//				}
//			}).start();
//			
			System.out.println("aaaaaaaaaaaaaaaaaaaaaaa");
		}
	}
}

