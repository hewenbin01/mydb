package cn.com.dao;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import cn.com.mydb.DBPool;
import cn.com.mydb.DbManager;

/** 
 * <p>项 目：MYDB</p>
 * <p>模 块：</p>
 * <p>描 述：</p>
 * <p>版 权: Copyright (c) 2016</p>
 * <p>公 司: 天玑科技</p>
 * @author hewenbin
 * @version 1.0 
 * 创建时间：2016年10月26日 下午1:41:47
 */
public class UserDao extends DbManager {

	@Before
	public void before() {
		System.out.println("…………初始化操作开始启动…………");
		new DBPool().init();
	}

	@Ignore
	public void destroy() {
		System.out.println("…………释放连接池…………");
		new DBPool().release();
	}
	
	@Test
	public void test(){
		for (int i = 0; i < 10; i++) {
			System.out.println(i);
		    Thread thread =	new Thread(new TestThread());
		    thread.start();
		}
	}
	
	public void  tt(){
		try {
			String sqlId0 = "sql.sql0";
			Map<String, Object> paramMap0 = new HashMap<String, Object>();
			paramMap0.put("username", "admin");
			Map<String, Map<String, Object>> map = new HashMap<String, Map<String, Object>>();
			map.put(sqlId0, paramMap0);
			
			
			Map<String, Object> res = executeSql(map);
			System.out.println(res);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

