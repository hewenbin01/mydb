package cn.com.mydb;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ResourceBundle;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.mchange.v2.c3p0.ComboPooledDataSource;

/** 
 * <p>项 目：MYDB</p>
 * <p>模 块：连接池</p>
 * <p>描 述：</p>
 * <p>版 权: Copyright (c) 2016</p>
 * <p>公 司: 天玑科技</p>
 * @author hewenbin
 * @version 1.0 
 * 创建时间：2016年10月26日 下午1:26:47
 */
public class DBPool {

	private static Log log = LogFactory.getLog(DBPool.class);
	
	private static ComboPooledDataSource cpds = null;
	private ResourceBundle resource = ResourceBundle.getBundle("dbinfo");
	
	/**
	 * 初始化连接池
	 */
	public void init() {
		// 建立数据库连接池
		String DRIVER_NAME = getConfigInfomation("MYSQL_DRIVER_NAME"); // 驱动器
		String DATABASE_URL = getConfigInfomation("MYSQL_DATABASE_URL"); // 数据库连接url
		String DATABASE_USER = getConfigInfomation("MYSQL_DATABASE_USER"); // 数据库用户名
		String DATABASE_PASSWORD = getConfigInfomation("MYSQL_DATABASE_PASSWORD"); // 数据库密码
		int Min_PoolSize = 0;
		int Max_PoolSize = 0;
		int Acquire_Increment = 0;
		int Initial_PoolSize = 0;
		int Idle_Test_Period = 0;// 每隔3000s测试连接是否可以正常使用
		String Validate = getConfigInfomation("VALIDATE");// 每次连接验证连接是否可用
		if (Validate.equals("")) {
			Validate = "false";
		}
		try {
			// 最小连接数
			Min_PoolSize = Integer.parseInt(getConfigInfomation("MIN_POOLSIZE"));
			// 增量条数
			Acquire_Increment = Integer.parseInt(getConfigInfomation("ACQUIRE_INCREMENT"));
			// 最大连接数
			Max_PoolSize = Integer .parseInt(getConfigInfomation("MAX_POOLSIZE"));
			// 初始化连接数
			Initial_PoolSize = Integer.parseInt(getConfigInfomation("INITIAL_POOLSIZE"));
			// 每隔3000s测试连接是否可以正常使用
			Idle_Test_Period = Integer.parseInt(getConfigInfomation("IDLE_TEST_PERIOD"));
		} catch (Exception ex) {
			log.error("…………读取数据库配置参数异常…………");
			ex.printStackTrace();
		}

		try {
			log.info("…………开始初始化连接池…………");
			cpds = new ComboPooledDataSource();
			cpds.setDriverClass(DRIVER_NAME); // 驱动器
			cpds.setJdbcUrl(DATABASE_URL); // 数据库url
			cpds.setUser(DATABASE_USER); // 用户名
			cpds.setPassword(DATABASE_PASSWORD); // 密码
			cpds.setInitialPoolSize(Initial_PoolSize); // 初始化连接池大小
			cpds.setMinPoolSize(Min_PoolSize); // 最少连接数
			cpds.setMaxPoolSize(Max_PoolSize); // 最大连接数
			cpds.setAcquireIncrement(Acquire_Increment); // 连接数的增量
			cpds.setIdleConnectionTestPeriod(Idle_Test_Period); // 测连接有效的时间间隔
			cpds.setTestConnectionOnCheckout(Boolean.getBoolean(Validate)); // 每次连接验证连接是否可用
			log.info("…………初始化连接池完成…………");
		} catch (Exception ex) {
			log.error("…………初始化连接池连接池异常…………");
			ex.printStackTrace();
		}
	}
	
	/**
	 * 获取数据库连接对象
	 * 
	 * @return
	 * @throws SQLException
	 */
	 static Connection getConnection() throws SQLException {
		Connection connection = null;
		try {
			if (cpds == null) {
				log.info("------连接池没有初始化，请先初始化连接池………………");
			}
			log.info("------开始获取连接………………");
			connection = cpds.getConnection();
			System.out.println("--------------------");
			log.info("------成功获取连接………………");
		} catch (SQLException ex) {
			System.out.println("+++++++++++++++++++++++++=");
			log.info("------获取连接异常………………");
			ex.printStackTrace();
			throw ex;
		}
		return connection;
	}

	/**
	 * 释放连接池
	 */
	public void release() {
		try {
			log.info("------释放连接池连接………………");
			if (cpds != null)
				cpds.close();
		} catch (Exception ex) {
			log.error("------释放连接池链接异常………………");
			ex.printStackTrace();
		}
	}
	
	/**
	 * 获取dbinfo配置信息，
	 * 
	 * @param itemIndex 参数名称
	 * @return 参数值
	 */
	private String getConfigInfomation(String itemIndex) {
		try {
			return resource.getString(itemIndex);
		} catch (Exception e) {
			return "";
		}
	}

}

