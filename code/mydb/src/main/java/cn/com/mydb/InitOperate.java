package cn.com.mydb;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


public class InitOperate {

	protected static Log log = LogFactory.getLog(InitOperate.class);

	/***
	 * 系统启动的初始化操作
	 */
	public void init() {
		log.info("…………初始化操作开始启动…………");
		new DBPool().init();
		DbManager.LoadSqlMap();

	}

	/**
	 * 系统关闭之前的销毁操作
	 */
	public void destroy() {
		log.info("…………释放连接池…………");
		new DBPool().release();
	}

}
