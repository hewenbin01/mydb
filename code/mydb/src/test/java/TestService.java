import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import cn.com.mydb.DbManager;

public class TestService {

	protected static Log log = LogFactory.getLog(TestService.class);

	/**
	 * 
	 * @param args
	 */
	public static void main1(String[] args) {
		try {
			// new TestService().getSql("sql1");
			// ClassLoader cl = TestService.class.getClassLoader();
			// String path =
			// cl.getResource("com/web/config/dbxml/sql.xml").toURI().getPath();
			// System.out.println(path);
			// Map<String, String> map=new HashMap<String, String>();
			// map.put("roleid", "000");
			// String sql = new TestService().getSql("sql1");
			// System.out.println(checkSql(sql, map));
			// sql = formatSql(sql);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		try {
			String sqlId0="sql0";
			Map<String, Object> paramMap0=new HashMap<String, Object>();
			paramMap0.put("roleid", "000");
			
			 String sqlId1="sql1";
			 Map<String, Object> paramMap1=new HashMap<String, Object>();
			 paramMap1.put("te_name", "名字1");
			 paramMap1.put("operId", "dxgl");
			 
			 String sqlId2="sql2";
			 Map<String, Object> paramMap2=new HashMap<String, Object>();
			 paramMap2.put("te_name", "名字2");
			 paramMap2.put("operId", "dongjl");
			 Map<String, Map<String, Object>> map=new HashMap<String, Map<String,Object>>();
			 map.put(sqlId0, paramMap0);
			 map.put(sqlId1, paramMap1);
			 map.put(sqlId2, paramMap2);
			 Map<String, Object> res= new DbManager().executeSql(map);
			 List<Map<String, Object>> res0=(List<Map<String, Object>>) res.get(sqlId0);
			 int res1=(Integer) res.get(sqlId1);
			 int res2=(Integer) res.get(sqlId2);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	

	/**
	 * 执行sql Map<sqlId,Map<paraName,paraValue>> Map<sqlId,Result>
	 * 
	 * @param list
	 * @return
	 * @throws SQLException
	 */
//	public void getRes(String sql, List<String> paramsList,
//			Map<String, Object> paramMap) throws SQLException {
//		/**
//		 * 根据sqlId 从配置文件中取sql，将参数关键字通过正则，替换成占位符;//分页查询的操作不写入sql，重新封装sql，不同的数据库
//		 * ，分页的方式不一样; 过滤首尾的空格，从第一个单词判断是哪种操作，如果有>1个的更新操作，则启动事务;
//		 * 根据sqlId,从sqlId_paras中取参数，放到一个数组中，每个参数的位置和sql语句中相同名称参数的位置相同;
//		 * 将参数放到sql中; 逐条执行; 将执行的结果再放到返回的Map中，key=sqlId; //TODO 能否返回查询的的字段名称
//		 * ArrayList<ArrayList<Map<ColumnName,value>>> res;
//		 * 结果集<一行数据<一个字段的数据<字段名称,对应字段的值>>>
//		 * 
//		 */
//		Connection conn = null;
//		try {
//			conn = DbManager.getConnection();
//			/* 启动事务 */
//			conn.setAutoCommit(false);
//			PreparedStatement stm = null;
//
//			/* 开始执行SQL */
//			stm = conn.prepareStatement(sql);
//			List<Map<String, Object>> rows = new ArrayList<Map<String, Object>>();
//			for (int i = 0; i < paramsList.size(); i++) {
//				stm.setObject(i + 1, paramMap.get(paramsList.get(i)));
//			}
//			ResultSet rs = stm.executeQuery();
//			ResultSetMetaData rsmd = rs.getMetaData();// rs为查询结果集
//			int columnCount = rsmd.getColumnCount();
//			while (rs.next()) {
//				Map<String, Object> row = new HashMap<String, Object>();
//				for (int i = 1; i <= columnCount; ++i) {
//					String name = rsmd.getColumnName(i).toLowerCase();
//					Object value = rs.getObject(i);
//					System.out.println(name + "：" + value);
//					row.put(name, value);
//				}
//				rows.add(row);
//			}
//
//			/* 提交事务 */
//			conn.commit();
//		} catch (SQLException e) {
//			if (conn != null) {
//				conn.rollback();
//			}
//			throw e;
//		}
//	}

	public static void test() throws SQLException {
//		Connection conn = DbManager.getConnection();
//		if (conn != null) {
//			Statement statement = conn.createStatement();
//			ResultSet rs = statement.executeQuery("select * from bk_sys_role");
//			int c = rs.getMetaData().getColumnCount();
//			while (rs.next()) {
//				log.info("库中有数据");
//				for (int i = 1; i <= c; i++) {
//					System.out.print(rs.getObject(i));
//				}
//			}
//			rs.close();
//		}
//		DbManager.release();
	}
}
